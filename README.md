## Gas Multiplexer Controller

This is the precursor of the TUmux for old hardware based on the NI Fieldpoint and compactFP controllers. This only supports an Emerson NGA 1000 gas analyser.

## License
This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact
For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).