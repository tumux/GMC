﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">Versie 3 is voor de cFP controllers, versie 2 voor de oudere FP controllers (in combinatie met LV 8.2).</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">&amp;1#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">&amp;1#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="support" Type="Folder" URL="../support">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="SubVIs" Type="Folder" URL="../SubVIs">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="controls" Type="Folder" URL="../controls">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="modules" Type="Folder" URL="../modules">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Drivers" Type="Folder" URL="../Drivers">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Documentation" Type="Folder" URL="../Documentation">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="GMC.vi" Type="VI" URL="../GMC.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="pointref.ctl" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Publish Data Subs/pointref.ctl"/>
				<Item Name="Init Publish.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish.vi"/>
				<Item Name="Init Publish (Bool).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish (Bool).vi"/>
				<Item Name="GLogos.dll" Type="Document" URL="/&lt;vilib&gt;/system/GLogos.dll"/>
				<Item Name="Validate Item Name.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Validate Item Name.vi"/>
				<Item Name="Init Publish (Double).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish (Double).vi"/>
				<Item Name="Init Publish (Int).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish (Int).vi"/>
				<Item Name="Init Publish (String).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish (String).vi"/>
				<Item Name="FP Get Configuration Info.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/FP Get Configuration Info.vi"/>
				<Item Name="ErrorConvert.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/SubVIs/ErrorConvert.vi"/>
				<Item Name="FP Check Error.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/SubVIs/FP Check Error.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Init Publish (Variant).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Init Publish Data.llb/Init Publish (Variant).vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="FP Write (Polymorphic).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Polymorphic).vi"/>
				<Item Name="FP Write (Float -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Float -IO).vi"/>
				<Item Name="FP Write (Boolean Array -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Boolean Array -IO).vi"/>
				<Item Name="FP Write (Boolean -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Boolean -IO).vi"/>
				<Item Name="FP Write (Float Array -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Float Array -IO).vi"/>
				<Item Name="FP Write (Float Array).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Float Array).vi"/>
				<Item Name="FP Write (Float).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Float).vi"/>
				<Item Name="FP Write (Boolean Array).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Boolean Array).vi"/>
				<Item Name="FP Write (Boolean).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Write.llb/FP Write (Boolean).vi"/>
				<Item Name="Write Publish Data.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data.vi"/>
				<Item Name="Write Publish Data (Bool).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data (Bool).vi"/>
				<Item Name="Write Publish Data (Double).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data (Double).vi"/>
				<Item Name="Write Publish Data (Int).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data (Int).vi"/>
				<Item Name="Write Publish Data (String).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data (String).vi"/>
				<Item Name="Write Publish Data (Variant).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Publish/Write Publish Data.llb/Write Publish Data (Variant).vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="File Exists__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists__ogtk.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="File Exists - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Array__ogtk.vi"/>
				<Item Name="Fit VI window to Largest Dec__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Fit VI window to Largest Dec__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Boolean Trigger__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/boolean/boolean.llb/Boolean Trigger__ogtk.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TUD Gas Multiplexer" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{B8A6814D-173F-4304-A7A7-D4F8BE7D56C1}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">TUD Gas Multiplexer</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[2].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">3</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/TUD Gas Multiplexer</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4624C767-8162-4709-95AC-2E786A8CC543}</Property>
				<Property Name="Bld_targetDestDir" Type="Path"></Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/TUD Gas Multiplexer</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/TUD Gas Multiplexer/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{A4CF4B30-8AE3-4451-85A2-2A2AD90F04B3}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/support</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/modules/GMC-cFP.channel.config.switch.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/modules/GMC-cFP.data.selector.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/modules/GMC-cFP.channel.config.dio.vi</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/modules/GMC-cFP.errorlogfile.vi</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/modules/errorlogfile.vi</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref"></Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/modules/read name.vi</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/modules/GMC-cFP.config.read.vi</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref"></Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/modules/GMC-cFP.settings.translate.vi</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Source</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref"></Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/modules/GMC-cFP.display.act-channel.vi</Property>
				<Property Name="Source[21].type" Type="Str">VI</Property>
				<Property Name="Source[22].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[22].itemID" Type="Ref">/My Computer/controls</Property>
				<Property Name="Source[22].type" Type="Str">Container</Property>
				<Property Name="Source[23].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[23].itemID" Type="Ref"></Property>
				<Property Name="Source[23].type" Type="Str">VI</Property>
				<Property Name="Source[24].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[24].itemID" Type="Ref"></Property>
				<Property Name="Source[24].type" Type="Str">VI</Property>
				<Property Name="Source[25].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[25].itemID" Type="Ref"></Property>
				<Property Name="Source[25].type" Type="Str">VI</Property>
				<Property Name="Source[26].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[26].itemID" Type="Ref"></Property>
				<Property Name="Source[26].type" Type="Str">VI</Property>
				<Property Name="Source[27].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[27].itemID" Type="Ref"></Property>
				<Property Name="Source[27].type" Type="Str">VI</Property>
				<Property Name="Source[28].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[28].itemID" Type="Ref"></Property>
				<Property Name="Source[28].type" Type="Str">VI</Property>
				<Property Name="Source[29].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[29].itemID" Type="Ref"></Property>
				<Property Name="Source[29].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Source</Property>
				<Property Name="Source[30].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[30].itemID" Type="Ref"></Property>
				<Property Name="Source[30].type" Type="Str">VI</Property>
				<Property Name="Source[31].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[31].itemID" Type="Ref"></Property>
				<Property Name="Source[31].type" Type="Str">VI</Property>
				<Property Name="Source[32].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[32].itemID" Type="Ref">/My Computer/Drivers/NGA2000</Property>
				<Property Name="Source[32].type" Type="Str">Container</Property>
				<Property Name="Source[33].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[33].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests</Property>
				<Property Name="Source[33].type" Type="Str">Container</Property>
				<Property Name="Source[34].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[34].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA Configure VISA.vi</Property>
				<Property Name="Source[34].type" Type="Str">VI</Property>
				<Property Name="Source[35].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[35].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA Extract Numbers 2 type 2 - subvi.vi</Property>
				<Property Name="Source[35].type" Type="Str">VI</Property>
				<Property Name="Source[36].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[36].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA measurements 2 - event.vi</Property>
				<Property Name="Source[36].type" Type="Str">VI</Property>
				<Property Name="Source[37].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[37].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA measurements 2old.vi</Property>
				<Property Name="Source[37].type" Type="Str">VI</Property>
				<Property Name="Source[38].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[38].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA measurements 2test.vi</Property>
				<Property Name="Source[38].type" Type="Str">VI</Property>
				<Property Name="Source[39].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[39].itemID" Type="Ref">/My Computer/Drivers/NGA2000/tests/NGA measurements 2test2.vi</Property>
				<Property Name="Source[39].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/SubVIs</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[40].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[40].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA Extract Numbers - subvi.vi</Property>
				<Property Name="Source[40].type" Type="Str">VI</Property>
				<Property Name="Source[41].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[41].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA Extract Numbers 2 - subvi.vi</Property>
				<Property Name="Source[41].type" Type="Str">VI</Property>
				<Property Name="Source[42].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[42].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA gas titles - subvi.vi</Property>
				<Property Name="Source[42].type" Type="Str">VI</Property>
				<Property Name="Source[43].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[43].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA gasquery.vi</Property>
				<Property Name="Source[43].type" Type="Str">VI</Property>
				<Property Name="Source[44].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[44].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA measurements 2.vi</Property>
				<Property Name="Source[44].type" Type="Str">VI</Property>
				<Property Name="Source[45].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[45].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA measurements 3.vi</Property>
				<Property Name="Source[45].type" Type="Str">VI</Property>
				<Property Name="Source[46].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[46].itemID" Type="Ref">/My Computer/Drivers/NGA2000/NGA measurements.vi</Property>
				<Property Name="Source[46].type" Type="Str">VI</Property>
				<Property Name="Source[47].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[47].itemID" Type="Ref">/My Computer/Drivers/NGA2000/Test NGA 2.vi</Property>
				<Property Name="Source[47].type" Type="Str">VI</Property>
				<Property Name="Source[48].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[48].itemID" Type="Ref">/My Computer/Drivers/NGA2000/Test NGA.vi</Property>
				<Property Name="Source[48].type" Type="Str">VI</Property>
				<Property Name="Source[49].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[49].itemID" Type="Ref"></Property>
				<Property Name="Source[49].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[49].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].type" Type="Str">Container</Property>
				<Property Name="Source[50].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[50].itemID" Type="Ref"></Property>
				<Property Name="Source[50].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[50].type" Type="Str">VI</Property>
				<Property Name="Source[51].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[51].itemID" Type="Ref"></Property>
				<Property Name="Source[51].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[51].type" Type="Str">VI</Property>
				<Property Name="Source[52].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[52].itemID" Type="Ref"></Property>
				<Property Name="Source[52].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[52].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/modules/OPC/init publish data.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/modules/OPC/GMC-cFP.opc.publishdatamodule.init.vi</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/SubVIs/Timer</Property>
				<Property Name="Source[8].type" Type="Str">Container</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/modules/Timer/Timing_login_Module.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">53</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="GMC-5" Type="FP Invalid">
		<Item Name="GMC-5" Type="Folder"/>
		<Item Name="Build Specifications" Type="FP Invalid">
			<Item Name="GMC-5" Type="FP Invalid"/>
		</Item>
		<Item Name="cFP-2200 @0" Type="FP Invalid">
			<Item Name="DIP Switch 1" Type="FP Invalid"/>
			<Item Name="DIP Switch 2" Type="FP Invalid"/>
			<Item Name="LED A" Type="FP Invalid"/>
			<Item Name="LED B" Type="FP Invalid"/>
			<Item Name="LED C" Type="FP Invalid"/>
			<Item Name="LED D" Type="FP Invalid"/>
			<Item Name="Power Source" Type="FP Invalid"/>
		</Item>
		<Item Name="cFP-DO-400 @1" Type="FP Invalid">
			<Item Name="All" Type="FP Invalid"/>
			<Item Name="Channel 0" Type="FP Invalid"/>
			<Item Name="Channel 1" Type="FP Invalid"/>
			<Item Name="Channel 2" Type="FP Invalid"/>
			<Item Name="Channel 3" Type="FP Invalid"/>
			<Item Name="Channel 4" Type="FP Invalid"/>
			<Item Name="Channel 5" Type="FP Invalid"/>
			<Item Name="Channel 6" Type="FP Invalid"/>
			<Item Name="Channel 7" Type="FP Invalid"/>
		</Item>
		<Item Name="Dependencies" Type="FP Invalid">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Boolean Trigger__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists - Array__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists__ogtk.vi" Type="FP Invalid"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="FP Invalid"/>
				<Item Name="Check Special Tags.vi" Type="FP Invalid"/>
				<Item Name="Clear Errors.vi" Type="FP Invalid"/>
				<Item Name="Convert property node font to graphics font.vi" Type="FP Invalid"/>
				<Item Name="Details Display Dialog.vi" Type="FP Invalid"/>
				<Item Name="DialogType.ctl" Type="FP Invalid"/>
				<Item Name="DialogTypeEnum.ctl" Type="FP Invalid"/>
				<Item Name="Error Code Database.vi" Type="FP Invalid"/>
				<Item Name="ErrorConvert.vi" Type="FP Invalid"/>
				<Item Name="ErrWarn.ctl" Type="FP Invalid"/>
				<Item Name="eventvkey.ctl" Type="FP Invalid"/>
				<Item Name="Find Tag.vi" Type="FP Invalid"/>
				<Item Name="Format Message String.vi" Type="FP Invalid"/>
				<Item Name="FP Check Error.vi" Type="FP Invalid"/>
				<Item Name="FP Get Configuration Info.vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean Array -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean Array).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float Array -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float Array).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Polymorphic).vi" Type="FP Invalid"/>
				<Item Name="General Error Handler Core CORE.vi" Type="FP Invalid"/>
				<Item Name="General Error Handler.vi" Type="FP Invalid"/>
				<Item Name="Get String Text Bounds.vi" Type="FP Invalid"/>
				<Item Name="Get Text Rect.vi" Type="FP Invalid"/>
				<Item Name="GetHelpDir.vi" Type="FP Invalid"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="FP Invalid"/>
				<Item Name="glogos.dll" Type="FP Invalid"/>
				<Item Name="Init Publish (Bool).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Double).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Int).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (String).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Variant).vi" Type="FP Invalid"/>
				<Item Name="Init Publish.vi" Type="FP Invalid"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="FP Invalid"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="FP Invalid"/>
				<Item Name="LVRectTypeDef.ctl" Type="FP Invalid"/>
				<Item Name="Not Found Dialog.vi" Type="FP Invalid"/>
				<Item Name="pointref.ctl" Type="FP Invalid"/>
				<Item Name="Search and Replace Pattern.vi" Type="FP Invalid"/>
				<Item Name="Set Bold Text.vi" Type="FP Invalid"/>
				<Item Name="Set String Value.vi" Type="FP Invalid"/>
				<Item Name="Simple Error Handler.vi" Type="FP Invalid"/>
				<Item Name="TagReturnType.ctl" Type="FP Invalid"/>
				<Item Name="Three Button Dialog CORE.vi" Type="FP Invalid"/>
				<Item Name="Three Button Dialog.vi" Type="FP Invalid"/>
				<Item Name="Trim Whitespace.vi" Type="FP Invalid"/>
				<Item Name="Validate Item Name.vi" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="FP Invalid"/>
				<Item Name="whitespace.ctl" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Bool).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Double).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Int).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (String).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Variant).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data.vi" Type="FP Invalid"/>
			</Item>
			<Item Name="GMC-cFP.channel.config.dio.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.channel.config.switch.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.channel.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.cluster.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.read.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.data.selector.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.display.act-channel.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.errorlogfile.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.fpio.build.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.fpremoteini.delete.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.opc.publishdatamodule.init.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.reschape boolean 1D array to 2D array.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.settings.translate.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.State.Control.2.ctl" Type="FP Invalid"/>
			<Item Name="NGA Extract Numbers 2 - subvi.vi" Type="FP Invalid"/>
			<Item Name="NGA measurements 3.vi" Type="FP Invalid"/>
			<Item Name="Timing_Command Control.ctl" Type="FP Invalid"/>
			<Item Name="Timing_Module.vi" Type="FP Invalid"/>
		</Item>
		<Item Name="GMC_3.0.vi" Type="FP Invalid"/>
	</Item>
	<Item Name="GMC-GG-3" Type="FP Invalid">
		<Item Name="GMC-GG-3" Type="Folder"/>
		<Item Name="Build Specifications" Type="FP Invalid">
			<Item Name="GMC-3" Type="FP Invalid"/>
			<Item Name="GMC_3 source" Type="FP Invalid"/>
		</Item>
		<Item Name="cFP-2200 @0" Type="FP Invalid">
			<Item Name="DIP Switch 1" Type="FP Invalid"/>
			<Item Name="DIP Switch 2" Type="FP Invalid"/>
			<Item Name="LED A" Type="FP Invalid"/>
			<Item Name="LED B" Type="FP Invalid"/>
			<Item Name="LED C" Type="FP Invalid"/>
			<Item Name="LED D" Type="FP Invalid"/>
			<Item Name="Power Source" Type="FP Invalid"/>
		</Item>
		<Item Name="cFP-DO-400 @1" Type="FP Invalid">
			<Item Name="All" Type="FP Invalid"/>
			<Item Name="Channel 0" Type="FP Invalid"/>
			<Item Name="Channel 1" Type="FP Invalid"/>
			<Item Name="Channel 2" Type="FP Invalid"/>
			<Item Name="Channel 3" Type="FP Invalid"/>
			<Item Name="Channel 4" Type="FP Invalid"/>
			<Item Name="Channel 5" Type="FP Invalid"/>
			<Item Name="Channel 6" Type="FP Invalid"/>
			<Item Name="Channel 7" Type="FP Invalid"/>
		</Item>
		<Item Name="cFP-DO-400 @2" Type="FP Invalid">
			<Item Name="All" Type="FP Invalid"/>
			<Item Name="Channel 0" Type="FP Invalid"/>
			<Item Name="Channel 1" Type="FP Invalid"/>
			<Item Name="Channel 2" Type="FP Invalid"/>
			<Item Name="Channel 3" Type="FP Invalid"/>
			<Item Name="Channel 4" Type="FP Invalid"/>
			<Item Name="Channel 5" Type="FP Invalid"/>
			<Item Name="Channel 6" Type="FP Invalid"/>
			<Item Name="Channel 7" Type="FP Invalid"/>
		</Item>
		<Item Name="Dependencies" Type="FP Invalid">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Boolean Trigger__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists - Array__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="FP Invalid"/>
				<Item Name="File Exists__ogtk.vi" Type="FP Invalid"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="FP Invalid"/>
				<Item Name="Check Special Tags.vi" Type="FP Invalid"/>
				<Item Name="Clear Errors.vi" Type="FP Invalid"/>
				<Item Name="Convert property node font to graphics font.vi" Type="FP Invalid"/>
				<Item Name="Details Display Dialog.vi" Type="FP Invalid"/>
				<Item Name="DialogType.ctl" Type="FP Invalid"/>
				<Item Name="DialogTypeEnum.ctl" Type="FP Invalid"/>
				<Item Name="Error Code Database.vi" Type="FP Invalid"/>
				<Item Name="ErrorConvert.vi" Type="FP Invalid"/>
				<Item Name="ErrWarn.ctl" Type="FP Invalid"/>
				<Item Name="eventvkey.ctl" Type="FP Invalid"/>
				<Item Name="Find Tag.vi" Type="FP Invalid"/>
				<Item Name="Format Message String.vi" Type="FP Invalid"/>
				<Item Name="FP Check Error.vi" Type="FP Invalid"/>
				<Item Name="FP Get Configuration Info.vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean Array -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean Array).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Boolean).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float Array -IO).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float Array).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Float).vi" Type="FP Invalid"/>
				<Item Name="FP Write (Polymorphic).vi" Type="FP Invalid"/>
				<Item Name="General Error Handler Core CORE.vi" Type="FP Invalid"/>
				<Item Name="General Error Handler.vi" Type="FP Invalid"/>
				<Item Name="Get String Text Bounds.vi" Type="FP Invalid"/>
				<Item Name="Get Text Rect.vi" Type="FP Invalid"/>
				<Item Name="GetHelpDir.vi" Type="FP Invalid"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="FP Invalid"/>
				<Item Name="glogos.dll" Type="FP Invalid"/>
				<Item Name="Init Publish (Bool).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Double).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Int).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (String).vi" Type="FP Invalid"/>
				<Item Name="Init Publish (Variant).vi" Type="FP Invalid"/>
				<Item Name="Init Publish.vi" Type="FP Invalid"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="FP Invalid"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="FP Invalid"/>
				<Item Name="LVRectTypeDef.ctl" Type="FP Invalid"/>
				<Item Name="Not Found Dialog.vi" Type="FP Invalid"/>
				<Item Name="pointref.ctl" Type="FP Invalid"/>
				<Item Name="Search and Replace Pattern.vi" Type="FP Invalid"/>
				<Item Name="Set Bold Text.vi" Type="FP Invalid"/>
				<Item Name="Set String Value.vi" Type="FP Invalid"/>
				<Item Name="Simple Error Handler.vi" Type="FP Invalid"/>
				<Item Name="TagReturnType.ctl" Type="FP Invalid"/>
				<Item Name="Three Button Dialog CORE.vi" Type="FP Invalid"/>
				<Item Name="Three Button Dialog.vi" Type="FP Invalid"/>
				<Item Name="Trim Whitespace.vi" Type="FP Invalid"/>
				<Item Name="Validate Item Name.vi" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="FP Invalid"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="FP Invalid"/>
				<Item Name="whitespace.ctl" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Bool).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Double).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Int).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (String).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data (Variant).vi" Type="FP Invalid"/>
				<Item Name="Write Publish Data.vi" Type="FP Invalid"/>
			</Item>
			<Item Name="GMC-cFP.channel.config.dio.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.channel.config.switch.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.channel.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.cluster.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.ctl" Type="FP Invalid"/>
			<Item Name="GMC-cFP.config.read.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.data.selector.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.display.act-channel.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.errorlogfile.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.fpio.build.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.fpremoteini.delete.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.opc.publishdatamodule.init.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.reschape boolean 1D array to 2D array.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.settings.translate.vi" Type="FP Invalid"/>
			<Item Name="GMC-cFP.State.Control.2.ctl" Type="FP Invalid"/>
			<Item Name="NGA Extract Numbers 2 - subvi.vi" Type="FP Invalid"/>
			<Item Name="NGA measurements 3.vi" Type="FP Invalid"/>
			<Item Name="Timing_Command Control.ctl" Type="FP Invalid"/>
			<Item Name="Timing_Module.vi" Type="FP Invalid"/>
		</Item>
		<Item Name="GMC_3.0.vi" Type="FP Invalid"/>
		<Item Name="GMC_3.lvproj_EDIT__GMC-GG-3__{C81852E4-EC10-45F2-9012-8DF6BB96F926}_GMC_3.0.html" Type="FP Invalid"/>
	</Item>
</Project>
